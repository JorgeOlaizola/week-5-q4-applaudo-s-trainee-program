import React, { useState } from 'react';
import useAuth from './adapters/hooks/useAuth';
import usePrevious from './adapters/hooks/usePrevious';
import Landing from './pages/Landing';
import Home from './pages/Home';
import Detail from './pages/Detail';
import LogIn from './pages/LogIn';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import './styles/globals.scss';

function App() {
  const [page, setPage] = useState('landing');
  const previous = usePrevious(page);
  const [getUser, logOut] = useAuth();
  function changePage(pageName) {
    setPage(pageName);
  }

  return (
    <div className="root">
      <Navbar changePage={changePage} user={getUser} previous={previous}/>
      {page === 'landing' && <Landing changePage={changePage} />}
      {page === 'home' && <Home changePage={changePage} />}
      {page === 'login' && <LogIn changePage={changePage} user={getUser} logOut={logOut}/>}
      {page !== 'home' && page !== 'landing' && page !== 'login' && <Detail changePage={changePage} id={page} user={getUser} previous={previous}/>}

      <Footer />
    </div>
  );
}

export default App;
