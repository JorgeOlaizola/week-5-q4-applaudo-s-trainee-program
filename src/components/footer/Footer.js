import React, { memo } from 'react';
import '../../styles/footer.scss';

function Footer() {
  return (
    <footer className='footer'>
      <span className='footer-item'>Videogames app</span>
      <span className='footer-item'>Created by Jorge Olaizola</span>
      <span className='footer-item'>
        Follow me on{' '}
        <a href='https://gitlab.com/JorgeOlaizola'>
          GitLab
        </a>
      </span>
    </footer>
  );
}

export default memo(Footer);
