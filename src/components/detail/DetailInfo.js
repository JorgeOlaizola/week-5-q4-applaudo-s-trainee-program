import React from 'react';
import { DEFAULT_IMG } from '../../adapters/globals';
import '../../styles/detail.scss';

export default function DetailInfo({
  name,
  image,
  genre,
  platforms,
  released,
  price
}) {
  return (
    <div className='detail-info-container'>
      <h2>{name}</h2>
      <div className='detail-info-container-separator'>
        <div className='detail-info-image'>
          <img className='detail-image' src={image ? image.formats.small.url : DEFAULT_IMG } alt={name} />
        </div>
        <div className='detail-info-text'>
          <h4>Genre</h4>
          <div className='detail-info-genres'>
            {genre && <span>{genre.name + ' '}</span>}
          </div>
          <h4>Platforms</h4>
          <div>
            {platforms &&
              platforms.length > 0 &&
              platforms.map(({ name, id }) => <span key={id}>{name + ' '}</span>)}
          </div>
          <h4>Release year</h4>
          <div>{released}</div>
          <button onClick={() => alert('Buy products is currently disabled. Try later!')} className='detail-info-buy'>Buy now for only ${price}</button>
        </div>
      </div>
    </div>
  );
}
