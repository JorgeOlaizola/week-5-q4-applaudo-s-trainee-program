import React, {
  useEffect,
  useState,
  useRef,
  useCallback
} from 'react';
import { throttle } from 'lodash';
import { commentsHandler, fetchComments } from '../../adapters/fetch';
import { SERVER_ERROR_MSG } from '../../adapters/globals';
import Pagination from '../videogames/Pagination';
import Comment from './Comment';

export default function Comments({ id, user }) {
  const [allComments, setAllComments] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [error, setError] = useState();
  const [input, setInput] = useState('');
  const commentsPerPage = 6;
  const userInfo = user();
  const errorContent = useRef();

  useEffect(() => {
    fetchComments(id)
      .then((response) => response.json())
      .then((data) => {
        data = data.filter(({ body }) => body.trim() !== '').reverse();
        setAllComments(data);
      })
      .catch(() => alert(SERVER_ERROR_MSG));
  }, []);

  function paginate(page) {
    setCurrentPage(page);
  }

  const indexOfLastComment = currentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  const currentComment = allComments.slice(indexOfFirstComment, indexOfLastComment);

  function handleSubmit() {
    if (input.trim() === '') {
      setError('Your comment cannot be empty');
      errorContent.current.style.display = 'block';
      return;
    }
    commentsHandler(id, input, userInfo.jwt)
      .then((response) => response.json())
      .then((data) => {
        setAllComments((prevComments) => {
          const comment = {
            body: data.body,
            user: {
              username: userInfo.user.username
            }
          };
          return [comment, ...prevComments];
        });
        setInput('');
        setError('');
        errorContent.current.style.display = 'none';
      })
      .catch(() => {
        setError('Something went wrong, try later');
        errorContent.current.style.display = 'block';
      });
  }

  const throttledFunction = useCallback(throttle(() => handleSubmit(), 2000), [input]);

  const throttleSubmit = (event) => {
    event.preventDefault();
    throttledFunction();
  };

  return (
    <div className='comments-container'>
      {userInfo.user ? (
        <>
        <div ref={errorContent} className='comments-form-error'>{error}</div>
        <form onSubmit={throttleSubmit} className='comments-form'>
          <input type='text' onChange={(event) => setInput(event.target.value)} value={input} name='body' placeholder='Send a comment!' />
          <input className='comments-form-submit' type='submit' value='Send' />
        </form>
        </>
      ) : (
        <div className='comments-form'>If you want to leave a comment, please log in.</div>
      )}
      {currentComment && currentComment.length > 0 ? (
        currentComment.map(
          ({ id, body, user }) => body !== '' && <Comment key={id} user={user.username} comment={body}></Comment>
        )
      ) : (
        <div className='comment'>No comments yet. Be the first one!</div>
      )}
      <Pagination
        itemsPerPage={commentsPerPage}
        totalItems={allComments.length}
        paginate={paginate}
        currentPage={currentPage}
        noScroll={true}
      />
    </div>
  );
}
