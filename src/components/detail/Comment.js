import React from 'react';

export default function Comment({ comment, user }) {
  return (
    <div className='comment'>
      <div className='comment-user'>{user}</div>
      <div className='comment-body'>{comment}</div>
    </div>
  );
}
