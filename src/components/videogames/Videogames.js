import React from 'react';
import '../../styles/videogames.scss';
import Videogame from './Videogame';

export default function Videogames({ videogames, loading, changePage }) {
  return (
    <div className='videogames-container'>
      {loading
        ? 'Loading...'
        : videogames &&
          videogames.map((game) => (
            <Videogame changePage={changePage} key={game.id} props={game}></Videogame>
          ))}
    </div>
  );
}
