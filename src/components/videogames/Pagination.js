import React, { useMemo } from 'react';
import '../../styles/videogames.scss';

export default function Pagination({
  itemsPerPage, totalItems, paginate, currentPage, noScroll
}) {
  const pageNumbers = [];
  const totalPages = useMemo(
    () => Math.ceil(totalItems / itemsPerPage),
    [itemsPerPage, totalItems]
  );

  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  const clickFunction = (page) => {
    paginate(page);
    if (!noScroll) window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  };

  return (
    <div className='pagination-container'>
      {currentPage !== 1 && totalItems > 0 && (
        <button className='pagination-item' onClick={() => clickFunction(currentPage - 1)}>
          ⬅
        </button>
      )}
      {pageNumbers.map((page) => (
        <button
          key={page}
          className={currentPage === page ? 'pagination-item-selected' : 'pagination-item'}
          onClick={() => clickFunction(page)}
        >
          {page}
        </button>
      ))}
      {currentPage !== pageNumbers[pageNumbers.length - 1] && totalItems > 0 && (
        <button className='pagination-item' onClick={() => clickFunction(currentPage + 1)}>
          ➡
        </button>
      )}
    </div>
  );
}
