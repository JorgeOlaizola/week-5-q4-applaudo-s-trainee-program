import React from 'react';
import { DEFAULT_IMG } from '../../adapters/globals';

export default function Videogame({ props, changePage }) {
  const { name, cover_art: image, id } = props;
  return (
    <div onClick={() => changePage(id)} className='videogame'>
      <h1 className='videogame-title'>{name}</h1>
      <img className='videogame-image' src={image ? image.formats.small.url : DEFAULT_IMG } alt={`${name}_${id}_img`}></img>
      <span className='videogame-button'>Click for more details ⤵</span>
    </div>
  );
}
