import React from 'react';
import '../../styles/navbar.scss';
import { PAGE_ICON } from '../../adapters/globals';
import User from './User';

export default function Navbar({ changePage, user, previous }) {
  return (
    <nav>
      <button className='button-back'onClick={() => changePage(previous)}>↩</button>
      <img onClick={() => changePage('landing')} className='icon' alt='page-icon' src={PAGE_ICON} />
      <User changePage={changePage} user={user} />
    </nav>
  );
}
