import React from 'react';
import '../../styles/user.scss';

export default function User({ changePage, user }) {
  return (
        <div className='user-container' onClick={() => changePage('login')}>
            <img src="https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg" className='user-img'/>
            { user().user ? (user().user.username || 'Log In') : 'Log In' }
        </div>
  );
}
