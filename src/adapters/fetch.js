import { BASE_URL } from './globals';
import { postCommentsOpts } from './utils';

export async function fetchGames(id) {
  return fetch(`${BASE_URL}/games/${id || ''}`);
}

export async function fetchComments(id) {
  return fetch(`${BASE_URL}/games/${id}/comments`);
}

export async function commentsHandler(gameId, body, token) {
  return fetch(`${BASE_URL}/games/${gameId}/comment`, postCommentsOpts(body, token));
}
