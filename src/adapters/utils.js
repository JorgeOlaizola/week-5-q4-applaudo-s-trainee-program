export function postOpts(data) {
  return {
    method: 'POST',
    body: JSON.stringify(data),
    headers: { 'Content-type': 'application/json; charseft=UFT-8' },
  };
}

export function postCommentsOpts(data, token) {
  return {
    method: 'POST',
    body: JSON.stringify({ body: data }),
    headers: {
      'Content-type': 'application/json; charseft=UFT-8',
      Authorization: `Bearer ${token}`,
    },
  };
}
