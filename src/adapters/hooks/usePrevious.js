import { useRef, useEffect } from 'react';

export default function usePrevious(prevPage) {
  const page = useRef();
  useEffect(() => {
    page.current = prevPage;
  }, [prevPage]);
  return page.current;
}
