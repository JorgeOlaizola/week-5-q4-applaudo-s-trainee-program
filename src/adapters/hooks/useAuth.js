import { logOut } from '../authentication';

export default function useAuth() {
  function getUser() {
    let user = localStorage.getItem('session');
    if (user) {
      user = JSON.parse(user);
      return user;
    }
    return {};
  }
  return [getUser, logOut];
}
