import { BASE_URL } from './globals';

export function logIn(opts) {
  return fetch(`${BASE_URL}/auth/local`, opts);
}

export function logOut() {
  localStorage.setItem('session', JSON.stringify({}));
}
