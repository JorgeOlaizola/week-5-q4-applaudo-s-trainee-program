import React, { useEffect, useState } from 'react';
import DetailInfo from '../components/detail/DetailInfo';
import Comments from '../components/detail/Comments';
import { SERVER_ERROR_MSG } from '../adapters/globals';
import { fetchGames } from '../adapters/fetch';
import '../styles/detail.scss';

export default function Detail({
  changePage, id, user, previous
}) {
  const [game, setGame] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetchGames(id)
      .then((response) => response.json())
      .then((data) => {
        setGame(data);
        setLoading(false);
      })
      .catch(() => alert(SERVER_ERROR_MSG));
  }, [id]);

  function clickFunction() {
    changePage(previous);
  }

  const {
    name,
    cover_art: image,
    genre,
    platforms,
    release_year: released,
    comments,
    price,
  } = game;
  return (
    <div className='detail-comments-container'>
      {loading ? (
        <div className='detail-container'>Loading</div>
      ) : (
        <div className='detail-container'>
          <button className='detail-back-button' onClick={clickFunction}>
            ⬅ Go Back
          </button>
          <DetailInfo
            name={name}
            image={image}
            genre={genre}
            platforms={platforms}
            released={released}
            price={price}
          />
        </div>
      )}
      {comments && <Comments id={id} user={user} comments={comments} />}
    </div>
  );
}
