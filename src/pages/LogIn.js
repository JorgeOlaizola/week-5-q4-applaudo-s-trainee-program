import React, { useState } from 'react';
import { postOpts } from '../adapters/utils';
import { logIn } from '../adapters/authentication';
import '../styles/user.scss';

export default function LogIn({ changePage, user, logOut }) {
  const [form, setForm] = useState({ identifier: '', password: '' });
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const userInfo = user();

  function handleChange(event) {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  }

  function responseHandler(data) {
    if (data.statusCode) return setError('Invalid username or password');
    localStorage.setItem('session', JSON.stringify(data));
    return changePage('home');
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (error !== '') setError('');
    setLoading(true);
    logIn(postOpts(form))
      .then((response) => response.json())
      .then((data) => {
        setLoading(false);
        responseHandler(data);
      })
      .catch(() => {
        setLoading(false);
        setError('Something went wrong');
      });
  }

  return !userInfo.user ? (
    <div className='login-container'>
      {loading && <div className='login-loading'>Sending request...</div>}
      {error && <div className='login-error'>{error}</div>}
      <h1 className='login-title'>Log In</h1>
      <form onSubmit={handleSubmit} className='login-form'>
        <input
          onChange={handleChange}
          className='login-form-input'
          type='text'
          name='identifier'
          placeholder='Username'
        ></input>
        <input
          onChange={handleChange}
          className='login-form-input'
          type='password'
          name='password'
          placeholder='Password'
        ></input>
        <input className='login-form-submit' type='submit' value='Log In'></input>
      </form>
    </div>
  ) : (
    <div className='login-container'>
    <h1 className='login-title'>Account</h1>
    <span className='login-user-item'>Firstname: {userInfo.user.firstName}</span>
    <span className='login-user-item'>Lastname: {userInfo.user.lastName}</span>
    <span className='login-user-item'>Email: {userInfo.user.email}</span>
    <span className='login-user-item'>Username: {userInfo.user.username}</span>
    <span className='login-user-item'>User ID: {userInfo.user.id}</span>
    <button className='login-button-logout' onClick={() => { logOut(); changePage('home'); }}>Log Out</button>
    </div>
  );
}
