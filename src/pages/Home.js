import React, { useState, useEffect } from 'react';
import Pagination from '../components/videogames/Pagination';
import Videogames from '../components/videogames/Videogames';
import { fetchGames } from '../adapters/fetch';
import '../styles/home.scss';

export default function Home({ changePage }) {
  const [videogames, setVideogames] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const videogamesPerPage = 8;
  useEffect(() => {
    let mounted = true;

    setLoading(true);
    fetchGames()
      .then((response) => {
        if (mounted) {
          return response.json();
        }
        return null;
      })
      .then((data) => {
        if (mounted) {
          setVideogames(data);
          setLoading(false);
        }
      });
    return () => {
      mounted = false;
    };
  }, []);

  function paginate(page) {
    setCurrentPage(page);
  }

  const indexOfLastVideogame = currentPage * videogamesPerPage;
  const indexOfFirstVideogame = indexOfLastVideogame - videogamesPerPage;
  const currentVideogame = videogames.slice(indexOfFirstVideogame, indexOfLastVideogame);
  return (
    <div className='home'>
      <Videogames videogames={currentVideogame} changePage={changePage} loading={loading} />
      <Pagination
        itemsPerPage={videogamesPerPage}
        totalItems={videogames.length}
        paginate={paginate}
        currentPage={currentPage}
      ></Pagination>
    </div>
  );
}
