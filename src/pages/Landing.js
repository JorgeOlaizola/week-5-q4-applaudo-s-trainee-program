import React from 'react';
import { PAGE_ICON } from '../adapters/globals';
import '../styles/landing.scss';

export default function Landing({ changePage }) {
  return (
    <div className='landing-container'>
      <h1 className='landing-title'>Welcome to the videogames App</h1>
      <img className='landing-logo' src={PAGE_ICON} alt='logo-image' />
      <hr />
      <button className='landing-button' onClick={() => changePage('home')}>
        Get started!
      </button>
    </div>
  );
}
