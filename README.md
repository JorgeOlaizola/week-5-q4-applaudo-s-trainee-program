# Videogames App - Week 5 Q4 Applaudo Trainee Program

Application that uses the Trainee Gamerbox api (https://trainee-gamerbox.herokuapp.com) to fetch videogames.

## Try it

Link to deploy: https://week-5-q4-applaudo-s-trainee-program.vercel.app/

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/week-5-q4-applaudo-s-trainee-program.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

Execute the following command to run the project in http://localhost:3000

```bash
  npm start
```
